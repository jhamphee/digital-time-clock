const container = document.querySelector(".main-container");
const clockContainer = document.querySelector("clock-container");
const clock = document.querySelector("#clock");

function twelveHourFormat() {
    let d = new Date();
    let time = d.toLocaleTimeString();
    clock.innerHTML = `${time}`;
}
function twentyFourHourFormat() {
    let d = new Date();
    let hours = putZero(d.getHours());
    let mins = putZero(d.getMinutes());
    let secs = putZero(d.getSeconds());
    clock.innerHTML = `${hours}:${mins}:${secs}`;
}
function putZero(time) {
    if(time < 10) {
        return `0${time}`;
    } else {
        return time;
    }
}
function digitalClock() {
    if(status) {
        return twentyFourHourFormat();
    } else {
        return twelveHourFormat();
    }
}

setInterval(digitalClock, 1000);


const formatBtn = document.querySelector(".format > button");
let status = true;
formatBtn.addEventListener("click", function(){
    status = !status;
});

// let clasic = document.querySelector(".classic")
const fontBtn = document.querySelector(".font > button");
fontBtn.addEventListener("click", function(){
  body.style.fontFamily = 'Abril Fatface';
});

let themeBtn = document.querySelector("#theme");
themeBtn.addEventListener("click", function(){
  container.style.backgroundColor = "#510A32";
  clockContainer.style.color = "white";
  clock.style.backgroundColor = "#EE4540";
  // alert();
});